<?php
declare(strict_types=1);

namespace Alex\BlogPost\Ui\DataProvider\Post;


use Luhta\MediaQueue\Model\Logger;
use Alex\BlogPost\Api\PostRepositoryInterface;
use Magento\Framework\Api\Filter;
use Magento\Framework\App\RequestInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Psr\Log\LoggerInterface;

class FormDataProvider extends AbstractDataProvider
{

    /** @var array */
    private $loadedData;

    protected $postRepository;

    /** @var RequestInterface */
    private $request;

    private $logger;

    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        RequestInterface $request,
        PostRepositoryInterface $postRepository,
        LoggerInterface $logger,
        array $meta = [],
        array $data = [])
    {

        $this->postRepository = $postRepository;
        $this->logger = $logger;
        $this->request = $request;

        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getData(): array
    {
        if ($this->loadedData) {
            return $this->loadedData;
        }

        try {
            $tagId = (int)$this->request->getParam($this->primaryFieldName);
            if ($tagId) {
                $tag = $this->postRepository->getById($tagId);

                $this->loadedData[$tag->getId()] = $tag->getData();
            }
        } catch (\Throwable $exception) {
            $this->logger->error(
                'Error when rendering Blog form.',
                ['message' => $exception->getMessage(), 'error_class' => \get_class($exception)]
            );
        } finally {
            return $this->loadedData ?? [];
        }
    }

    /**
     * {@inheritdoc}
     */
    public function addFilter(Filter $filter)
    {
        return null;
    }
}
