<?php

declare(strict_types=1);

namespace Alex\BlogPost\Ui\Component\Listing\Column;

use Luhta\Tags\Api\Data\TagInterface;
use Magento\Framework\Escaper;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class PageActions
 */
class EditActions extends Column
{
    /** Url path */
    const CMS_URL_PATH_EDIT = 'alex_blog/blog/edit';
    const CMS_URL_PATH_DELETE = 'alex_blog/blog/delete';
    const ENTITY_ID = 'id';

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var string
     */
    private $editUrl;

    /**
     * @var Escaper
     */
    private $escaper;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     * @param string $editUrl
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        Escaper $escaper,
        array $components = [],
        array $data = [],
        $editUrl = self::CMS_URL_PATH_EDIT
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->editUrl = $editUrl;
        $this->escaper = $escaper;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {

//                var_dump($item);
//                die();
                $name = $this->getData('name');
                if (isset($item[self::ENTITY_ID])) {
                    $item[$name]['edit'] = [
                        'href' => $this->urlBuilder->getUrl(
                            $this->editUrl,
                            [self::ENTITY_ID => $item[self::ENTITY_ID]]
                        ),
                        'label' => __('Edit'),
                        '__disableTmpl' => true,
                    ];

                    $tagName = $this->escaper->escapeHtml($item['title']);

                    $item[$name]['delete'] = [
                        'href' => $this->urlBuilder->getUrl(
                            self::CMS_URL_PATH_DELETE,
                            [self::ENTITY_ID => $item[self::ENTITY_ID]]
                        ),
                        'label' => __('Delete'),
                        'confirm' => [
                            'title' => __('Delete %1', $tagName),
                            'message' => __(
                                'Are you sure you want to delete a post?',
                                $tagName

                            ),
                            '__disableTmpl' => true,
                        ],
                        'post' => true,
                        '__disableTmpl' => true,
                    ];
                }
            }
        }

        return $dataSource;
    }
}
