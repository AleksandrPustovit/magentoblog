<?php
declare(strict_types=1);

namespace Alex\BlogPost\Service\Rest\User;
use Alex\BlogPost\Service\Rest\User\UserFactory;

class ResponseProcessor
{
    private $userFactory;

    public function __construct(UserFactory $userFactory){
        $this->userFactory = $userFactory;
    }

    public function execute($response)
    {
        $responseUserInfo = $response['data'];
        /** @var User $user */
        $user = $this->userFactory->create();
        $user->setId($responseUserInfo['id']);
        $user->setName($responseUserInfo['name']);
        $user->setEmail($responseUserInfo['email']);

        return $user;
    }

}
