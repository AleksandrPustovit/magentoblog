<?php
declare(strict_types=1);

namespace Alex\BlogPost\Service\Rest;
use Alex\BlogPost\Service\Rest\GetPosts\RequestProcessor;
use Alex\BlogPost\Service\Rest\GetPosts\ResponseProcessor;
use Alex\BlogPost\Service\Rest\RequestSender;
use Alex\BlogPost\Service\Rest\User\User;
use Alex\BlogPost\Api\Data\GenericPostInterface;
use Alex\BlogPost\Service\Rest\GetPosts\Post;
use Alex\BlogPost\Service\Rest\GetPosts\PostFactory;

/**
 * Class PostManagements
 * @package Alex\BlogPost\Service\Rest
 */
class PostManagements
{
    /**
     * @var \Alex\BlogPost\Service\Rest\RequestSender
     */
    private $connector;
    /**
     * @var RequestProcessor
     */
    private $requestProcessor;
    /**
     * @var ResponseProcessor
     */
    private $responseProcessor;
    /**
     * @var PostFactory
     */
    private $postFactory;
    /**
     * @var
     */
    private $logger;

    /**
     * GetPosts constructor.
     * @param \Alex\BlogPost\Service\Rest\RequestSender $connector
     * @param RequestProcessor $requestProcessor
     * @param ResponseProcessor $responseProcessor
     * @param PostFactory $postFactory
     * @param $logger
     */
    public function __construct(
        RequestSender $connector,
        RequestProcessor $requestProcessor,
        ResponseProcessor $responseProcessor,
        PostFactory $postFactory,
        $logger)
    {
        $this->connector = $connector;
        $this->requestProcessor = $requestProcessor;
        $this->responseProcessor = $responseProcessor;
        $this->postFactory = $postFactory;
        $this->logger = $logger;
    }

    /**
     * @param array $param
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($param = []){
        $response = $this->connector->makeRequest('posts',['query' => $param],'GET');
        $final = $this->responseProcessor->execute($response);
        return $final;
    }

    /**
     * @param User $user
     * @param Post $post
     * @return Post
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function create(User $user, Post $post){
        $postData = ['title' => $post->getTitle(), 'body' => $post->getBody()];
        $url = 'users/'.$user->getId().'/posts/';
        $postInfo = $response = $this->connector->makeRequest($url,['json' => $postData],'POST');
        $postData = $postInfo['data'];
        $newPost = $this->postFactory->create();
        $newPost->setData($postData);
        return $newPost;
    }

    /**
     * @param GenericPostInterface $post
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function update( GenericPostInterface $post){
        $postData = ['title' => $post->getTitle(), 'body' => $post->getBody()];
        $url = 'posts/' . $post->getPostId();
         $this->connector->makeRequest($url,['json' => $postData],'PUT');
    }

    /**
     * @param $postId
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function delete($postId){
        $url = 'posts/' . $postId;
        $this->connector->makeRequest($url,[],'DELETE');
    }


}
