<?php
declare(strict_types=1);

namespace Alex\BlogPost\Service\Rest;


use Alex\BlogPost\Service\Rest\GetPosts\RequestProcessor;
use Alex\BlogPost\Service\Rest\User\ResponseProcessor;

class UserManagements
{
    /** @var RequestSender  */
    private $connector;
    /** @var RequestProcessor  */
    private $requestProcessor;
    /** @var ResponseProcessor  */
    private $responseProcessor;

    /**
     * GetPosts constructor.
     * @param $connector
     * @param $requestProcessor
     * @param $responseProcessor
     * @param $logger
     */
    public function __construct(
        RequestSender $connector,
        RequestProcessor $requestProcessor,
        ResponseProcessor $responseProcessor
    )
    {
        $this->connector = $connector;
        $this->requestProcessor = $requestProcessor;
        $this->responseProcessor = $responseProcessor;
    }

    /**
     * @param $userId
     * @param array $param
     * @return User\User
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($userId, $param = [])
    {
        $response = $this->connector->makeRequest('users/' . $userId, $param, 'GET');
        $final = $this->responseProcessor->execute($response);
        return $final;
    }

    /**
     * @param User\User $user
     * @return User\User
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function create(\Alex\BlogPost\Service\Rest\User\User $user)
    {
        $userData = [
            "name" => $user->getName(),
            "email" => $user->getEmail(),
            "gender" =>  $user->getGender(),
            "status" => "active"
        ];
        $response = $this->connector->makeRequest('users/', ['json' => $userData], 'POST');
        $final = $this->responseProcessor->execute($response);
        return $final;
    }

}
