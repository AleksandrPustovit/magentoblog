<?php
declare(strict_types=1);

namespace Alex\BlogPost\Service\Rest\GetPosts;


class PaginationLink
{
    private $previous;
    private $current;
    private $next;

    /**
     * PaginationLink constructor.
     * @param $previous
     * @param $current
     * @param $next
     */
    public function __construct($previous, $current, $next)
    {
        $this->previous = $previous;
        $this->current = $current;
        $this->next = $next;
    }


    /**
     * @return mixed
     */
    public function getPrevious()
    {
        return $this->previous;
    }

    /**
     * @param mixed $previous
     */
    public function setPrevious($previous): void
    {
        $this->previous = $previous;
    }

    /**
     * @return mixed
     */
    public function getCurrent()
    {
        return $this->current;
    }

    /**
     * @param mixed $current
     */
    public function setCurrent($current): void
    {
        $this->current = $current;
    }

    /**
     * @return mixed
     */
    public function getNext()
    {
        return $this->next;
    }

    /**
     * @param mixed $next
     */
    public function setNext($next): void
    {
        $this->next = $next;
    }



}
