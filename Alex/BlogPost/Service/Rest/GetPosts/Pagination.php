<?php
declare(strict_types=1);

namespace Alex\BlogPost\Service\Rest\GetPosts;


class Pagination
{

    private $total;
    private $pages;
    private $page;
    private $limit;
    private PaginationLink $links;

    /**
     * @return mixed
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param mixed $total
     */
    public function setTotal($total): void
    {
        $this->total = $total;
    }

    /**
     * @return mixed
     */
    public function getPages()
    {
        return $this->pages;
    }

    /**
     * @param mixed $pages
     */
    public function setPages($pages): void
    {
        $this->pages = $pages;
    }

    /**
     * @return mixed
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param mixed $page
     */
    public function setPage($page): void
    {
        $this->page = $page;
    }

    /**
     * @return mixed
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param mixed $limit
     */
    public function setLimit($limit): void
    {
        $this->limit = $limit;
    }

    /**
     * @return PaginationLink
     */
    public function getLinks(): PaginationLink
    {
        return $this->links;
    }

    /**
     * @param PaginationLink $links
     */
    public function setLinks(PaginationLink $links): void
    {
        $this->links = $links;
    }




}
