<?php
declare(strict_types=1);

namespace Alex\BlogPost\Service\Rest\GetPosts;


class Post extends \Magento\Framework\DataObject
{
    public const ID = 'id';
    public const USER_ID = 'user_id';
    public const TITLE = 'title';
    public const BODY = 'body';


    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }



    /**
     * @return mixed
     */
    public function getUserId() :int
    {
        return $this->getData(self::USER_ID);
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        return $this->setData(self::USER_ID, $userId);
    }

    /**
     * @return mixed
     */
    public function getTitle(): string
    {
        return $this->getData(self::TITLE);
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * @return mixed
     */
    public function getBody() :string
    {
        return $this->getData(self::BODY);
    }

    /**
     * @param mixed $body
     */
    public function setBody($body)
    {
        return $this->setData(self::BODY, $body);
    }



}
