<?php
declare(strict_types=1);

namespace Alex\BlogPost\Service\Rest\GetPosts;

use Alex\BlogPost\Service\Rest\GetPosts\PaginationLink;
use Alex\BlogPost\Service\Rest\GetPosts\PaginationLinkFactory;
use Alex\BlogPost\Service\Rest\GetPosts\Pagination;
use Alex\BlogPost\Service\Rest\GetPosts\PaginationFactory;
use Alex\BlogPost\Service\Rest\GetPosts\PostFactory;


class ResponseProcessor
{

    private $paginationFactory;
    private $paginationLinkFactory;
    private $postFactory;


    /**
     * ResponseProcessor constructor.
     */
    public function __construct(
        PostFactory $postFactory,
        PaginationFactory $paginationFactory,
        PaginationLinkFactory $paginationLinkFactory
    )
    {
        $this->postFactory = $postFactory;
        $this->paginationFactory = $paginationFactory;
        $this->paginationLinkFactory = $paginationLinkFactory;
    }

    public function execute($response)
    {

        $responsePaginationInfo = $response['meta']['pagination'];

        /** @var Pagination $pagination */
        $pagination = $this->paginationFactory->create();

        /** @var PaginationLink $paginationLink */
        $paginationLink = $this->paginationLinkFactory->create([
                'previous' => $responsePaginationInfo['links']['previous'],
                'current' => $responsePaginationInfo['links']['current'],
                'next' => $responsePaginationInfo['links']['next'],
            ]
        );


        $pagination->setTotal($responsePaginationInfo['total']);
        $pagination->setPages($responsePaginationInfo['pages']);
        $pagination->setPage($responsePaginationInfo['page']);
        $pagination->setLimit($responsePaginationInfo['limit']);


        $pagination->setLinks($paginationLink);

        $posts = [];

        foreach ($response['data'] as $datum) {
            /** @var Post $tmpPost */
            $tmpPost = $this->postFactory->create();

            $tmpPost->setId($datum['id']);
            $tmpPost->setUserId($datum['user_id']);
            $tmpPost->setTitle($datum['title']);
            $tmpPost->setBody($datum['body']);

            $posts[] = $tmpPost;

        }

        return ['pagination' => $pagination, 'items' => $posts];

    }

}
