<?php
declare(strict_types=1);

namespace Alex\BlogPost\Service\Rest;
use Alex\BlogPost\Api\ServiceInterface;
use GuzzleHttp\Client;
use GuzzleHttp\ClientFactory;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\ResponseFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Webapi\Rest\Request;

use  Alex\BlogPost\Model\Post;
use  Alex\BlogPost\Model\PostFactory;



class RequestSender implements ServiceInterface
{
    /** @var \Alex\BlogPost\Model\Config  */
    private $config;
    /** @var ClientFactory  */
    private $clientFactory;
    /** @var ResponseFactory  */
    private $responseFactory;

    /**
     * RequestSender constructor.
     * @param \Alex\BlogPost\Model\Config $config
     * @param ClientFactory $clientFactory
     * @param ResponseFactory $responseFactory
     */
    public function __construct(
        \Alex\BlogPost\Model\Config $config,
        ClientFactory $clientFactory,
        ResponseFactory $responseFactory
    ){
        $this->config = $config;
        $this->clientFactory = $clientFactory;
        $this->responseFactory = $responseFactory;
    }

    public function makeRequest($uriEndpoint, $params = [], $requestMethod = self::POST): array
    {
        /** @var \GuzzleHttp\Client  $client */
        $client = $this->clientFactory->create(['config' => [
            'base_uri' => self::API_REQUEST_URI,
        ]]);
        $header = [
            'Authorization' => 'Bearer ' . $this->config->getApiToken(),
            'Accept'        => 'application/json',
        ];

        $data = [
            'headers' => $header,
        ];
        $data = array_merge($data,$params);

        try {
            $response = $client->request(
                $requestMethod,
                $uriEndpoint,
                $data
            );
        } catch (GuzzleException $exception) {
           if($exception->getCode() ==  404){
               throw new NoSuchEntityException(__('Object not found'));
           }
            throw new \Exception("Wrong request");

        }
        if ($response->getBody()) {
            $result = json_decode( (string)$response->getBody(),true);
            return $result??[];
        }

        return [];
    }



}
