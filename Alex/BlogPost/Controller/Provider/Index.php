<?php
declare(strict_types=1);

namespace Alex\BlogPost\Controller\Provider;


use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Alex\BlogPost\Service\Rest\PostManagements;
use Alex\BlogPost\Service\Rest\UserManagements;
use Alex\BlogPost\Model\PostFactory;
use Alex\BlogPost\Api\PostServiceProviderInterface;

class Index implements \Magento\Framework\App\Action\HttpPostActionInterface, \Magento\Framework\App\Action\HttpGetActionInterface
{

    /** @var PostServiceProviderInterface */
    private $postServiceProvider;
    /** @var PostManagements */
    private $postManagements;
    /** @var \Magento\Framework\View\Result\PageFactory */
    protected $resultPageFactory;
    /**
     * @var RequestInterface
     */
    private $request;
    /** @var UserManagements */
    private $userManagements;
    /** @var PostFactory */
    private $postFactory;

    /**
     * Index constructor.
     * @param PostServiceProviderInterface $postServiceProvider
     * @param PostManagements $postManagements
     * @param UserManagements $userManagements
     * @param PostFactory $postFactory
     * @param JsonFactory $resultJsonFactory
     * @param RequestInterface $request
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        PostServiceProviderInterface $postServiceProvider,
        PostManagements $postManagements,
        UserManagements $userManagements,
        PostFactory $postFactory,
        JsonFactory $resultJsonFactory,
        RequestInterface $request,
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory)
    {
        $this->postServiceProvider = $postServiceProvider;
        $this->postManagements = $postManagements;
        $this->userManagements = $userManagements;
        $this->postFactory = $postFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->request = $request;
    }


    public function execute()
    {

        if ($this->request->isPost()) {
            $message = $this->request->getParam('comment', null);
            $newPost = $this->postFactory->create();
            $newPost->setTitle('title');
            $newPost->setBody($message);
            $result = $this->postServiceProvider->createPost($newPost);

            $resultJson = $this->resultJsonFactory->create();
            return $resultJson->setData($result);

        } else if ($this->request->isGet()) {
            $page = (int)$this->request->getParam('page', 1);

            $result = $this->postServiceProvider->getPage($page);
            $resultJson = $this->resultJsonFactory->create();
            return $resultJson->setData($result);

        }

    }


}
