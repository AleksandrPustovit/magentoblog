<?php
declare(strict_types=1);

namespace Alex\BlogPost\Controller\Adminhtml\Blog;

use Alex\BlogPost\Api\PostRepositoryInterface;
use Alex\BlogPost\Api\PostServiceProviderInterface;
use Magento\Backend\App\Action;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Exception\LocalizedException;
use Psr\Log\LoggerInterface;
use Alex\BlogPost\Model\PostFactory;

/**
 * Class Save
 * @package Alex\BlogPost\Controller\Adminhtml\Blog
 */
class Save extends Action implements HttpPostActionInterface
{
    /** @var LoggerInterface */
    private $logger;
    /** @var PostServiceProviderInterface */
    private $postServiceProvider;
    /** @var PostRepositoryInterface */
    private $postRepository;
    /** @var PostFactory */
    private $postFactory;

    /**
     * Save constructor.
     * @param PostFactory $postFactory
     * @param LoggerInterface $logger
     * @param Action\Context $context
     * @param PostRepositoryInterface $postRepository
     * @param PostServiceProviderInterface $postServiceProvider
     */
    public function __construct(
        PostFactory $postFactory,
        LoggerInterface $logger,
        Action\Context $context,
        PostRepositoryInterface $postRepository,
        PostServiceProviderInterface $postServiceProvider
    )
    {
        $this->postFactory = $postFactory;
        $this->logger = $logger;
        $this->postRepository = $postRepository;
        $this->postServiceProvider = $postServiceProvider;

        parent::__construct($context);
    }

    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();


        if ($data) {
            $id = $this->getRequest()->getParam('id');

            try {
                if ($id) {
                    $postLocalData = $this->postRepository->getById($id);
                    $postLocalData->setData($data);
                    $this->postServiceProvider->updatePost($postLocalData);
                } else {
                    $newPost = $this->postFactory->create();
                    $newPost->setData($data);
                    $this->postServiceProvider->createPost($newPost);
                }
                $this->messageManager->addSuccessMessage(__('You saved the Post.'));

            } catch (\Throwable $e) {
                $this->messageManager->addErrorMessage(__('This post no longer exists.'));

                return $resultRedirect->setPath('*/*/');
            } finally {
                if ($id) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
                }
            }
        }

        return $resultRedirect->setPath('*/*/');
    }
}
