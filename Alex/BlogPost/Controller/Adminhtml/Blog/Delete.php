<?php
declare(strict_types=1);

namespace Alex\BlogPost\Controller\Adminhtml\Blog;


use Alex\BlogPost\Api\PostRepositoryInterface;
use Alex\BlogPost\Api\PostServiceProviderInterface;
use Magento\Backend\App\Action;
use Magento\Framework\App\Action\HttpPostActionInterface;

/**
 * Class Delete
 * @package Alex\BlogPost\Controller\Adminhtml\Blog
 */
class Delete extends Action implements HttpPostActionInterface
{
    /**
     * @var PostServiceProviderInterface
     */
    private $postServiceProvider;
    /**
     * @var  PostRepositoryInterface
     */
    private $postRepository;

    /**
     * Delete constructor.
     * @param PostRepositoryInterface $postRepository
     * @param Action\Context $context
     * @param PostServiceProviderInterface $postServiceProvider
     */
    public function __construct(
        PostRepositoryInterface $postRepository,
        Action\Context $context,
        PostServiceProviderInterface $postServiceProvider
    ) {
        parent::__construct($context);
        $this->postRepository = $postRepository;
        $this->postServiceProvider = $postServiceProvider;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();

        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('id');
        // 2. Initial checking
        if ($id) {
            try {
                $postLocalData = $this->postRepository->getById($id);
                $this->postServiceProvider->removePostByPostID($postLocalData->getPostId());
                // display success message
                $this->messageManager->addSuccessMessage(__('You deleted the post.'));
                // go to grid
                return $resultRedirect->setPath('*/*/index');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        }
        $this->messageManager->addErrorMessage(__('We can\'t find a post to delete.'));

        return $resultRedirect->setPath('*/*/index');
    }
}
