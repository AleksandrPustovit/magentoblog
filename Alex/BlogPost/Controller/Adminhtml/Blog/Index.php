<?php
declare(strict_types=1);

namespace Alex\BlogPost\Controller\Adminhtml\Blog;
use Magento\Framework\Controller\ResultFactory;

/**
 * Class Index
 * @package Alex\BlogPost\Controller\Adminhtml\Blog
 */
class Index extends \Magento\Backend\App\Action
{
    public function execute()
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        return $resultPage;
    }

}
