<?php
declare(strict_types=1);

namespace Alex\BlogPost\Controller\Adminhtml\Blog;


use Luhta\Tags\Api\Data\TagInterface;
use Alex\BlogPost\Api\PostRepositoryInterface;
use Magento\Backend\App\Action;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class Edit
 * @package Alex\BlogPost\Controller\Adminhtml\Blog
 */
class Edit extends Action implements HttpGetActionInterface
{
    /** @var PageFactory */
    protected $resultPageFactory;
    /** @var PostRepositoryInterface */
    private $postRepository;

    /**
     * Edit constructor.
     * @param Action\Context $context
     * @param PageFactory $resultPageFactory
     * @param PostRepositoryInterface $postRepository
     */
    public function __construct(
        Action\Context $context,
        PageFactory $resultPageFactory,
        PostRepositoryInterface $postRepository
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->postRepository = $postRepository;
    }


    public function execute()
    {
        $id = $this->getRequest()->getParam(TagInterface::ID);
        if ($id) {
            try {
                $this->postRepository->getById((int)$id);
            } catch (NoSuchEntityException $e) {

                $this->messageManager->addErrorMessage(__('This post no longer exists.'));
                /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();

                return $resultRedirect->setPath('*/*/index');

            }
        }

        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend(__('Editing Post'));

        return $resultPage;
    }
}

