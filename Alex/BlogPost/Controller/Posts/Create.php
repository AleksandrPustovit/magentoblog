<?php
declare(strict_types=1);

namespace Alex\BlogPost\Controller\Posts;

use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\JsonFactory;

class Create implements \Magento\Framework\App\Action\HttpPostActionInterface
{
    /**
     * @var PageFactory
     */
    private $pageFactory;

    /**
     * @var RequestInterface
     */
    private $request;

    protected $resultJsonFactory;

    /**
     * @param PageFactory $pageFactory
     * @param RequestInterface $request
     */
    public function __construct(PageFactory $pageFactory,
                                RequestInterface $request,
                                JsonFactory $resultJsonFactory
    )
    {
        $this->pageFactory = $pageFactory;
        $this->request = $request;
        $this->resultJsonFactory = $resultJsonFactory;
    }

    public function execute()
    {
        $firstParam = $this->request->getParam('comment', null);
//        var_dump($firstParam);

        $result = $this->resultJsonFactory->create();
        $result->setData(['result' => 111]);
        $result->setData(['orifinam ' => $firstParam]);
        die();

        return $result;
        // TODO: Implement execute() method.
    }
}
