<?php
declare(strict_types=1);

namespace Alex\BlogPost\Controller\Posts;


class Index  implements \Magento\Framework\App\Action\HttpGetActionInterface
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory)
    {
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
//        echo "fff";

        return $this->resultPageFactory->create();
    }
}
