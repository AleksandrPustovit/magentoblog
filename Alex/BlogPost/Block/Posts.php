<?php
declare(strict_types=1);

namespace Alex\BlogPost\Block;

use Magento\Framework\View\Element\Template;

/**
 * Class Posts
 * @package Alex\BlogPost\Block
 */
class Posts extends Template
{
    public function __construct(
        Template\Context $context, array $data = [])
    {
        parent::__construct($context, $data);
    }

//    public function getPosts(){
//
//        $rr = [];
//        $rr['data'] = [
//            ["title" => "aaa","user_id" => 555],
//            ["title" => "bbb","user_id" => 4444],
//        ];
////        var_dump($rr["data"]);
////        die();
////        $posts = [
////            ["title" => "aaa"],
////            ["title" => "bbb"],
////        ];
//        return $rr["data"];
//    }

}
