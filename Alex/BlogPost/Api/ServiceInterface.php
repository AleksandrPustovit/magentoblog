<?php
declare(strict_types=1);
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is released under commercial license by Lamia Oy.
 *
 * @copyright Copyright (c) Lamia Oy (https://lamia.fi)
 *
 */

namespace Alex\BlogPost\Api;

/**
 * Interface ServiceInterface
 * @package Alex\BlogPost\Api
 */
interface ServiceInterface
{
    public const POST   = 'post';
    public const GET    = 'get';
    public const PUT    = 'put';
    public const PATCH  = 'patch';
    public const DELETE = 'delete';
    public const API_REQUEST_URI = 'https://gorest.co.in/public/v1/';


    /**
     * Make API call
     *
     * @param $uriEndpoint
     * @param array $params
     * @param string $requestMethod
     * @return array Response body from API call
     */
    public function makeRequest($uriEndpoint, $params = [], $requestMethod = self::POST): array;



}
