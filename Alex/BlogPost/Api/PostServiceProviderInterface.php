<?php
declare(strict_types=1);
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is released under commercial license by Lamia Oy.
 *
 * @copyright Copyright (c) Lamia Oy (https://lamia.fi)
 *
 */

namespace Alex\BlogPost\Api;


use Alex\BlogPost\Api\Data\GenericPostInterface;

interface PostServiceProviderInterface
{
    /** Upfate post local and in external syste,
     * @param GenericPostInterface $post
     * @return mixed
     */
    public function updatePost(GenericPostInterface $post);

    /** Remove post local and in external system
     * @param $postId
     * @return mixed
     */
    public function removePostByPostID($postId);

    /** Create post locally and in external system
     * @param GenericPostInterface $message
     * @return mixed
     */
    public function createPost(\Alex\BlogPost\Api\Data\GenericPostInterface $message);

    /** Get page for FE
     * @param int $page
     * @return array
     */
    public function getPage($page = 1): array;

}
