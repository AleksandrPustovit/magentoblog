<?php
declare(strict_types=1);
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is released under commercial license by Lamia Oy.
 *
 * @copyright Copyright (c) Lamia Oy (https://lamia.fi)
 *
 */

namespace Alex\BlogPost\Api;

/**
 * Interface Post
 * @package Alex\BlogPost\Api
 */
interface Post
{
    /**
     * @return int
     */
    public function getId():int;

    /**
     * @return int
     */
    public function getUserId():int;

    /**
     * @return string
     */
    public function getTitle(): string;

    /**
     * @return string
     */
    public function getBody(): string;


}
