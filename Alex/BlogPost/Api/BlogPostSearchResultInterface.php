<?php
declare(strict_types=1);
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is released under commercial license by Lamia Oy.
 *
 * @copyright Copyright (c) Lamia Oy (https://lamia.fi)
 *
 */

namespace Alex\BlogPost\Api;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface BlogPostSearchResultInterface
 * @package Alex\BlogPost\Api
 */
interface BlogPostSearchResultInterface extends SearchResultsInterface
{

    /**
     * @return \Alex\BlogPost\Api\Post[]
     */
    public function getItems();

    /**
     * @param \Alex\BlogPost\Api\Post[]
     * @return void
     */
    public function setItems(array $items);

}
