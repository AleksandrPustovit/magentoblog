<?php
declare(strict_types=1);
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is released under commercial license by Lamia Oy.
 *
 * @copyright Copyright (c) Lamia Oy (https://lamia.fi)
 *
 */

namespace Alex\BlogPost\Api;


use Alex\BlogPost\Api\Data\PostInterface;

/**
 * Interface PostRepositoryInterface
 * @package Alex\BlogPost\Api
 */
interface PostRepositoryInterface
{
    /** Get post by id
     * @param $id
     * @return PostInterface
     */
    public function getById($id): \Alex\BlogPost\Api\Data\PostInterface;

    /**
     * Get post by post_id in external system
     * @param $postID
     * @return PostInterface
     */
    public function getByPostId($postID): \Alex\BlogPost\Api\Data\PostInterface;

    /**
     * Save post
     * @param PostInterface $post
     * @return mixed
     */
    public function save(PostInterface $post);

    /**
     * Delete post
     * @param PostInterface $post
     * @return bool
     */
    public function delete(PostInterface $post): bool;

    /**
     * Get collection
     * @param \Magento\Framework\Api\SearchCriteriaInterface|null $searchCriteria
     * @return BlogPostSearchResultInterface
     */
    public
    function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria = null): \Alex\BlogPost\Api\BlogPostSearchResultInterface;


}
