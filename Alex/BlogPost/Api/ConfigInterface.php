<?php
declare(strict_types=1);
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is released under commercial license by Lamia Oy.
 *
 * @copyright Copyright (c) Lamia Oy (https://lamia.fi)
 *
 */

namespace Alex\BlogPost\Api;

/**
 * Interface ConfigInterface
 * @package Alex\BlogPost\Api
 */
interface ConfigInterface
{
    public const BASE_PATH = 'blog_post/blog_settings/';
    public const TOKEN = 'token';
    public const EMAIL = 'user_email';
    public const USER_NAME = 'user_name';
    public const USER_GENDER = 'user_gender';
    public const USER_ID = 'user_id';

    /**
     * @return mixed
     */
    public function getUserName();

    /**
     * @return mixed
     */
    public function getUserEmail();

    /**
     * @return mixed
     */
    public function getUserId();

    /**
     * @return mixed
     */
    public function getUserGender();

    /**
     * @return mixed
     */
    public function getApiToken();

    /**
     * Because api is not stable and someone could remove our user,
     * we have to set user id if we register new one.
     * @param $id
     * @return mixed
     */
    public function setUserId($id);


}
