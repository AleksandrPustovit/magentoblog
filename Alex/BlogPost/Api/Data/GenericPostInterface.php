<?php
declare(strict_types=1);
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is released under commercial license by Lamia Oy.
 *
 * @copyright Copyright (c) Lamia Oy (https://lamia.fi)
 *
 */

namespace Alex\BlogPost\Api\Data;


/** Interface for all post instances
 * Interface GenericPostInterface
 * @package Alex\BlogPost\Api\Data
 */
interface GenericPostInterface
{
    /** Get post id
     * @return int
     */
    public function getPostId(): int;

    /** Set post id
     * @param $postId
     * @return $this
     */
    public function setPostId($postId): self;

    /** Get title
     * @return string
     */
    public function getTitle(): string;

    /** Set title
     * @param $title
     * @return $this
     */
    public function setTitle($title): self;

    /** Get body
     * @return string
     */
    public function getBody(): string;

    /** Set body
     * @param $body
     * @return $this\
     */
    public function setBody($body): self;

}
