<?php
declare(strict_types=1);
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is released under commercial license by Lamia Oy.
 *
 * @copyright Copyright (c) Lamia Oy (https://lamia.fi)
 *
 */
namespace Alex\BlogPost\Api\Data;

/** Post interface for db
 * Interface PostInterface
 * @package Alex\BlogPost\Api\Data
 */
interface PostInterface extends GenericPostInterface
{
    public const ID = 'id';
    public const USER_ID = 'user_id';
    public const TITLE = 'title';
    public const BODY = 'body';
    public const POST_ID = 'post_id';


    /** Get id
     * @return mixed
     */
    public function getId();

    /** Set id
     * @param $id
     * @return $this
     */
    public function setId($id): self;

    /**
     * Get user id
     * @return int
     */
    public function getUserId(): int;

    /**
     * Set user it
     * @param $userId
     * @return $this
     */
    public function setUserId($userId): self;

}
