<?php
declare(strict_types=1);

namespace Alex\BlogPost\Model\Service;

use Alex\BlogPost\Api\Data\GenericPostInterface;
use Alex\BlogPost\Service\Rest\PostManagements;
use Alex\BlogPost\Service\Rest\UserManagements;
use Alex\BlogPost\Service\Rest\GetPosts\PostFactory;
use Alex\BlogPost\Service\Rest\User\UserFactory;
use Alex\BlogPost\Api\Data\PostInterfaceFactory;
use Alex\BlogPost\Api\PostRepositoryInterface;
use Alex\BlogPost\Api\ConfigInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Alex\BlogPost\Api\PostServiceProviderInterface;

/**
 * Class PostServiceProvider
 * @package Alex\BlogPost\Model\Service
 */
class PostServiceProvider implements PostServiceProviderInterface
{
    /** @var ConfigInterface  */
    private $config;
    /** @var UserFactory  */
    private $userFactory;
    /** @var UserManagements  */
    private $userManagements;
    /** @var PostManagements  */
    private $postManagements;
    /** @var PostFactory  */
    private $postFactory;
    /** @var PostInterfaceFactory  */
    private $postInterfaceFactory;
    /** @var PostRepositoryInterface  */
    private $postRepository;


    /**
     * PostServiceProvider constructor.
     * @param ConfigInterface $config
     * @param UserFactory $userFactory
     * @param PostInterfaceFactory $postInterfaceFactory
     * @param PostRepositoryInterface $postRepository
     * @param PostFactory $postFactory
     * @param UserManagements $userManagements
     * @param PostManagements $postManagements
     */
    public function __construct(
        ConfigInterface $config,
        UserFactory $userFactory,
        PostInterfaceFactory $postInterfaceFactory,
        PostRepositoryInterface $postRepository,
        PostFactory $postFactory,
        UserManagements $userManagements,
        PostManagements $postManagements
    )
    {
        $this->config = $config;
        $this->userFactory = $userFactory;
        $this->postInterfaceFactory = $postInterfaceFactory;
        $this->postRepository = $postRepository;
        $this->postFactory = $postFactory;
        $this->userManagements = $userManagements;
        $this->postManagements = $postManagements;
    }

    /**
     * @inheriDoc
     */
    public function updatePost(GenericPostInterface $post)
    {
        $this->postManagements->update($post);
        $this->postRepository->save($post);

    }

    /**
     * @inheriDoc
     */
    public function removePostByPostID($postId)
    {
        $post = $this->postRepository->getByPostId($postId);
        $this->postManagements->delete($postId);
        $this->postRepository->delete($post);
    }

    /**
     * @inheriDoc
     */
    public function createPost(\Alex\BlogPost\Api\Data\GenericPostInterface $message)
    {
        $user = $this->getUser();

        $newPost = $this->postFactory->create();
        $newPost->setTitle($message->getTitle());
        $newPost->setBody($message->getBody());
        $newPostFromRest = $this->postManagements->create($user, $newPost);

        $this->savePostLocal($newPostFromRest);

        return $newPostFromRest;
    }

    /**
     * @inheriDoc
     */
    public function getPage($page = 1): array
    {
        $page = $page > 0 ? $page : 1;
        $response = $this->postManagements->get(['page' => $page]);
        $pageData = [];
        $links = $response['pagination']->getLinks();
        $pageData['pagination'] = [
            'next' => $this->formatPageURL($links->getNext()),
            'current' => $this->formatPageURL($links->getCurrent()),
            'previous' => $this->formatPageURL($links->getPrevious()),
        ];
        $pageData['pagination']['ddd'] = $page;
        $pageData['posts'] = [];
        foreach ($response['items'] as $item) {
            $pageData['posts'][] = [
                'title' => $item->getTitle(),
                'body' => $item->getBody(),
            ];
        }

        return $pageData;
    }

    /** Save post locally
     * @param $post
     */
    private function savePostLocal($post)
    {
        $forDb = $this->postInterfaceFactory->create();
        $forDb->setBody($post->getBody());
        $forDb->setTitle($post->getTitle());
        $forDb->setUserId($post->getUserId());
        $forDb->setPostId($post->getId());
        $this->postRepository->save($forDb);
    }

    /**
     * Because user could be removed from API we are checking that user is exist and user_id is correct.
     * Otherwise we create a new user.
     * @return \Alex\BlogPost\Service\Rest\User\User
     * @throws \Exception
     */
    private function getUser()
    {
        $configUserId = $this->config->getUserId();
        $email = $this->config->getUserEmail();
        $userName = $this->config->getUserName();
        $userGender = $this->config->getUserGender();

        if (!$email || !$userName || !$userGender) {
            throw new \Exception('Wrong settings');
        }
        if ($configUserId) {
            try {
                $user = $this->userManagements->get($configUserId);
            } catch (NoSuchEntityException $exception) {
                $user = $this->createUserFromConfig();
            }
        } else {
            $user = $this->createUserFromConfig();
        }
        return $user;

    }

    /** Create user from configuration
     * and saving his id.
     * @return \Alex\BlogPost\Service\Rest\User\User
     * @throws NoSuchEntityException
     */
    private function createUserFromConfig(): \Alex\BlogPost\Service\Rest\User\User
    {
        $email = $this->config->getUserEmail();
        $userName = $this->config->getUserName();
        $userGender = $this->config->getUserGender();

        $newUser = $this->userFactory->create();

        $newUser->setEmail($email);
        $newUser->setName($userName);
        $newUser->setGender($userGender);
        $user = $this->userManagements->create($newUser);
        $this->config->setUserId($user->getId());

        return $user;

    }

    /** Show only page number
     * @param $url
     * @return mixed|string
     */
    private function formatPageURL($url)
    {
        if (!$url) {
            return '';
        }
        preg_match('/page=(\d+)/', $url, $matches);
        return $matches[1];

    }

}
