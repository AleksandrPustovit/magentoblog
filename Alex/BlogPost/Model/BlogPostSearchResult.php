<?php
declare(strict_types=1);

namespace Alex\BlogPost\Model;


use Alex\BlogPost\Api\BlogPostSearchResultInterface;
use Magento\Framework\Api\SearchResults;
class BlogPostSearchResult extends SearchResults implements BlogPostSearchResultInterface
{

}
