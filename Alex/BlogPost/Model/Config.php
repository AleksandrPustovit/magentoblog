<?php
declare(strict_types=1);

namespace Alex\BlogPost\Model;

use Alex\BlogPost\Api\ConfigInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Class Config
 * @package Alex\BlogPost\Model
 */
class Config implements ConfigInterface
{
    /** @var ScopeConfigInterface */
    private $scopeConfig;
    /** @var \Magento\Config\Model\ResourceModel\Config */
    private $resourceConfig;
    /** @var \Magento\Framework\App\Cache\TypeListInterface */
    private $cacheTypeList;

    /**
     * Config constructor.
     * @param \Magento\Config\Model\ResourceModel\Config $resourceConfig
     * @param ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     */
    public function __construct(
        \Magento\Config\Model\ResourceModel\Config $resourceConfig,
        ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->resourceConfig = $resourceConfig;
        $this->cacheTypeList = $cacheTypeList;
    }

    /**
     * {@inheritdoc}
     */
    public function getUserName()
    {
        return $this->getValue(self::USER_NAME);
    }

    /**
     * {@inheritdoc}
     */
    public function getUserEmail()
    {
        return $this->getValue(self::EMAIL);
    }

    /**
     * {@inheritdoc}
     */
    public function getUserId()
    {
        return $this->getValue(self::USER_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function getUserGender()
    {
        return $this->getValue(self::USER_GENDER);
    }

    /**
     * {@inheritdoc}
     */
    public function getApiToken()
    {
        return $this->getValue(self::TOKEN);
    }

    /**
     * {@inheritdoc}
     */
    protected function getValue($key)
    {
        return $this->scopeConfig->getValue(self::BASE_PATH . $key);
    }

    /**
     * {@inheritdoc}
     */
    public function setUserId($id)
    {
        $this->resourceConfig->saveConfig(self::BASE_PATH . self::USER_ID, $id);
        $this->cacheTypeList->cleanType(\Magento\Framework\App\Cache\Type\Config::TYPE_IDENTIFIER);
        $this->cacheTypeList->cleanType(\Magento\PageCache\Model\Cache\Type::TYPE_IDENTIFIER);
    }

}
