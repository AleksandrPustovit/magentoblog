<?php
declare(strict_types=1);

namespace Alex\BlogPost\Model;

use Alex\BlogPost\Api\Data\PostInterface;
use Alex\BlogPost\Api\Data\PostInterfaceFactory;
use Alex\BlogPost\Model\ResourceModel\Post;
use Alex\BlogPost\Model\ResourceModel\Post\CollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Alex\BlogPost\Api\BlogPostSearchResultInterfaceFactory;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;
use Alex\BlogPost\Api\PostRepositoryInterface;

/**
 * Class PostRepository
 * @package Alex\BlogPost\Model
 */
class PostRepository implements PostRepositoryInterface
{

    private $blogPostSearchResultInterfaceFactory;
    private $collectionFactory;
    private $postFactory;
    private $resource;
    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    private $criteriaBuilder;


    /**
     * PostRepository constructor.
     * @param CollectionFactory $collectionFactory
     * @param PostFactory $postFactory
     * @param Post $resource
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        BlogPostSearchResultInterfaceFactory $blogPostSearchResultInterfaceFactory,
        \Magento\Framework\Api\SearchCriteriaBuilder $criteriaBuilder,
        CollectionFactory $collectionFactory,
        PostFactory $postFactory,
        Post $resource,
        CollectionProcessorInterface $collectionProcessor
    )
    {
        $this->blogPostSearchResultInterfaceFactory = $blogPostSearchResultInterfaceFactory;
        $this->collectionFactory = $collectionFactory;
        $this->criteriaBuilder = $criteriaBuilder;
        $this->postFactory = $postFactory;
        $this->resource = $resource;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * {@inheritdoc}
     */
    public
    function getById($id): \Alex\BlogPost\Api\Data\PostInterface
    {
        $info = $this->postFactory->create();
        $this->resource->load($info, $id);
        if (!$info->getId()) {
            throw new NoSuchEntityException(__('Unable to find data with  "%1"', $id));
        }
        return $info;
    }

    /**
     * {@inheritdoc}
     */
    public function getByPostId($postID): \Alex\BlogPost\Api\Data\PostInterface
    {
        $collection = $this->collectionFactory->create();
        $collection->addFieldToFilter('post_id', $postID);
        $collection->setPageSize(1);
        $item = $collection->getFirstItem();
        return $item;
    }

    /**
     * {@inheritdoc}
     */
    public function save(PostInterface $post)
    {
        $this->resource->save($post);
    }

    /**
     * {@inheritdoc}
     */
    public function delete(PostInterface $post): bool
    {
        try {
            $this->resource->delete($post);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public
    function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria = null): \Alex\BlogPost\Api\BlogPostSearchResultInterface
    {
        $collection = $this->collectionFactory->create();
        $this->collectionProcessor->process($searchCriteria, $collection);
        $searchResults = $this->blogPostSearchResultInterfaceFactory->create();
        $searchResults->setTotalCount($collection->getSize());
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        return $searchResults;
    }

}
