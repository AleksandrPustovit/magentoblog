<?php
declare(strict_types=1);

namespace Alex\BlogPost\Model\ResourceModel\Post;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'post_id';


    protected function _construct()
    {
        $this->_init('Alex\BlogPost\Model\Post', 'Alex\BlogPost\Model\ResourceModel\Post');
    }

}
