<?php
declare(strict_types=1);

namespace Alex\BlogPost\Model\ResourceModel;

class Post extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    protected function _construct()
    {
        $this->_init('alex_blog_post', 'id');
    }

}
