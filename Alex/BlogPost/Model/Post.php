<?php
declare(strict_types=1);

namespace Alex\BlogPost\Model;
use  Alex\BlogPost\Api\Data\PostInterface;
/**
 * @method \Alex\BlogPost\Model\ResourceModel\Post getResource()
 * @method \Alex\BlogPost\Model\ResourceModel\Post\Collection getCollection()
 */
class Post extends \Magento\Framework\Model\AbstractModel implements \Alex\BlogPost\Api\Data\PostInterface,
    \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'alex_blogpost_post';
    protected $_cacheTag = 'alex_blogpost_post';
    protected $_eventPrefix = 'alex_blogpost_post';



    protected function _construct()
    {
        $this->_init('Alex\BlogPost\Model\ResourceModel\Post');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setId($id): PostInterface
    {
        return $this->setData(self::ID, $id);
    }



    /**
     * {@inheritdoc}
     */
    public function getUserId() :int
    {
        return $this->getData(self::USER_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setUserId($userId): PostInterface
    {
        return $this->setData(self::USER_ID, $userId);
    }

    /**
     * {@inheritdoc}
     */
    public function getTitle(): string
    {
        return $this->getData(self::TITLE);
    }

    /**
     * {@inheritdoc}
     */
    public function setTitle($title): PostInterface
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * {@inheritdoc}
     */
    public function getBody() :string
    {
        return $this->getData(self::BODY);
    }

    /**
     * {@inheritdoc}
     */
    public function setBody($body): PostInterface
    {
        return $this->setData(self::BODY, $body);
    }

    /**
     * {@inheritdoc}
     */
    public function getPostId() :int
    {
        return (int)$this->getData(self::POST_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setPostId($postId): PostInterface
    {
        return $this->setData(self::POST_ID, $postId);
    }



}
