The BlogPost module provides functionality to create, edit, delete posts from https://gorest.co.in/ api.
For the beginning you need to provide access token and user details in
Store-Config-> Blog config -> Blog settings.
Please note, that user_id is not required field if the user is a new one.
Also this field will be updated automatically if the user will be registered again,
because the user could be deleted in the api.

Admin page is located in Marketing -> Blog -> Blog management
There you can create, edit and delete posts.

Sorry for JQuery in FE and simple styles.
